'use strict'

var echo = require('./echo');
var assert = require('assert');

describe('test case', function() {
    it('message', function(done){
        var message = 'Hello from GitLab Serverless';
        echo();
        assert.equal(message, echo(message));
        done();
    })
});
